# Ionic Token Auth

Ionic/Angular library for authenticating users using tokens. See also [Laravel Token Auth](https://gitlab.com/inoby-sk/laravel-token-auth).

## Documenation

Learn more [here](projects/ionic-token-auth/README.md)
