import { Component } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { forgotPassword, resetPassword } from '../../state/auth.actions';
import { AuthPage } from '../auth.page';

@Component({
  selector: 'inoby-forgot-password',
  templateUrl: './forgot-password.page.html',
  styleUrls: ['./forgot-password.page.css'],
})
export class ForgotPasswordPage extends AuthPage {
  form = this.fb.group({
    email: new FormControl<string>('', [Validators.email, Validators.required]),
  });

  handleReset(e: SubmitEvent) {
    e.preventDefault();

    const { email } = this.form.getRawValue();

    if (email) {
      this.store.dispatch(forgotPassword({ data: { email } }));
    }
  }
}
