import { Component } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormErrorService } from '../../services/form-error.service';
import { resetPassword } from '../../state/auth.actions';
import { AuthPage } from '../auth.page';

@Component({
  selector: 'inoby-reset-password',
  templateUrl: './reset-password.page.html',
  styleUrls: ['./reset-password.page.css'],
})
export class ResetPasswordPage extends AuthPage {
  form = this.fb.group({
    token: new FormControl<string>('', [Validators.required]),
    email: new FormControl<string>('', [Validators.email, Validators.required]),
    password: new FormControl<string>('', [Validators.required]),
    password_confirmation: new FormControl<string>('', [Validators.required]),
  });

  constructor(store: Store, fb: FormBuilder, router: Router, formErrors: FormErrorService) {
    super(store, fb, router, formErrors);

    const email: string | null = this.router.getCurrentNavigation()?.extras.state?.['email'] ?? null;
    const token: string | null = this.router.getCurrentNavigation()?.extras.state?.['token'] ?? null;

    if (!email || !token) {
      // TODO: show error notification
      this.router.navigate(['/']);
    }

    this.form.patchValue({ email, token });
  }

  handleReset(e: SubmitEvent) {
    e.preventDefault();

    const { email, token, password, password_confirmation } = this.form.getRawValue();

    if (email && token && password && password_confirmation) {
      this.store.dispatch(resetPassword({ data: { email, token, password, password_confirmation } }));
    }
  }
}
