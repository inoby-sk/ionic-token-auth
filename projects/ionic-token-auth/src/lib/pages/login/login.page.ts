import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { login } from '../../state/auth.actions';
import { AuthPage } from '../auth.page';

@Component({
  selector: 'inoby-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.css'],
})
export class LoginPage extends AuthPage {
  form = this.fb.group({
    email: new FormControl<string>('', [Validators.email, Validators.required]),
    password: new FormControl<string>('', [Validators.required]),
  });

  handleLogin(e: SubmitEvent) {
    e.preventDefault();

    const { email, password } = this.form.getRawValue();

    if (email && password) {
      this.store.dispatch(login({ data: { email, password } }));
    }
  }
}
