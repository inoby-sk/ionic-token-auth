import { Component } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { register } from '../../state/auth.actions';
import { AuthPage } from '../auth.page';

@Component({
  selector: 'inoby-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.css'],
})
export class RegisterPage extends AuthPage {
  form = this.fb.group({
    name: new FormControl<string>('', [Validators.required]),
    email: new FormControl<string>('', [Validators.email, Validators.required]),
    password: new FormControl<string>('', [Validators.required]),
    password_confirmation: new FormControl<string>('', [Validators.required]),
  });

  handleLogin(e: SubmitEvent) {
    e.preventDefault();

    const { name, email, password, password_confirmation } = this.form.getRawValue();

    if (name && email && password && password_confirmation) {
      this.store.dispatch(register({ data: { name, email, password, password_confirmation } }));
    }
  }
}
