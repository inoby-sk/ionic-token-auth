import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { changePassword } from '../../state/auth.actions';
import { AuthPage } from '../auth.page';

@Component({
  selector: 'inoby-change-password',
  templateUrl: './change-password.page.html',
  styleUrls: ['./change-password.page.css'],
})
export class ChangePasswordPage extends AuthPage {
  form = this.fb.group({
    old_password: new FormControl<string>('', [Validators.required]),
    password: new FormControl<string>('', [Validators.required]),
    password_confirmation: new FormControl<string>('', [Validators.required]),
  });

  handleSubmit(e: SubmitEvent) {
    e.preventDefault();

    const { old_password, password, password_confirmation } = this.form.getRawValue();

    if (old_password && password && password_confirmation) {
      this.store.dispatch(changePassword({ data: { old_password, password, password_confirmation } }));
    }
  }
}
