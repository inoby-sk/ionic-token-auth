import { Component, OnDestroy } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { FormErrorService } from '../services/form-error.service';
import { clearErrors } from '../state/auth.actions';

@Component({ template: '' })
export abstract class AuthPage implements OnDestroy {
  constructor(protected store: Store, protected fb: FormBuilder, protected router: Router, public formErrors: FormErrorService) {}

  ngOnDestroy(): void {
    // this.store.dispatch(clearErrors());
  }
}
