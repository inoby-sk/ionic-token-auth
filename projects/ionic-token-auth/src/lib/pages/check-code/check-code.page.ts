import { Component, OnDestroy } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { Store } from '@ngrx/store';
import { FormErrorService } from '../../services/form-error.service';
import { checkResetCode } from '../../state/auth.actions';
import { AuthPage } from '../auth.page';

@Component({
  selector: 'inoby-check-reset-code',
  templateUrl: './check-code.page.html',
  styleUrls: ['./check-code.page.css'],
})
export class CheckResetCodePage extends AuthPage {
  form = this.fb.group({
    email: new FormControl<string | null>(null, [Validators.email, Validators.required]),
    code: new FormControl<string>('', [Validators.required]),
  });

  constructor(store: Store, fb: FormBuilder, router: Router, formErrors: FormErrorService, private toast: ToastController) {
    super(store, fb, router, formErrors);

    const email: string | null = this.router.getCurrentNavigation()?.extras.state?.['email'] ?? null;

    if (!email) {
      this.handleEmpty();
    }

    this.form.patchValue({ email });
  }

  handleSubmit(e: SubmitEvent) {
    e.preventDefault();

    const { email, code } = this.form.getRawValue();

    if (email && code) {
      this.store.dispatch(checkResetCode({ data: { email, code } }));
    } else {
      this.handleEmpty();
    }
  }

  subtitle() {
    return 'Enter the verification code from email we have sent you to ' + this.form.getRawValue().email + '.';
  }

  private async handleEmpty() {
    const toast = await this.toast.create({
      message: 'An error occurred. Try again',
      duration: 3000,
      cssClass: '',
    });

    this.router.navigate(['/forgot-password']);

    await toast.present();
  }
}
