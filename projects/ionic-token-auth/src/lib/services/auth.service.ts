import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  CheckResetCodePayload,
  CheckResetCodeResponse,
  ForgotPasswordPayload,
  ForgotPasswordResponse,
  LoginPayload,
  LoginResponse,
  LogoutResponse,
  MeResponse,
  RegisterPayload,
  RegisterResponse,
  ResetPasswordPayload,
  ResetPasswordResponse,
} from '../models/auth.model';
import { BehaviorSubject, map, skipWhile, switchMap } from 'rxjs';
import { User } from '../models/user.model';
import { Store } from '@ngrx/store';
import { selectAuthenticatedUser } from '../state/auth.selectors';
import { TokenStorageService } from './token-storage.service';
import { ChangePasswordPayload, ChangePasswordResponse } from '../models/auth.model';
import { authContext, AUTH_CONFIG } from '../providers';

export interface AuthConfig {
  baseUrl?: string;
  routes: {
    login: string;
    me: string;
    logout: string;
    register: string;
    forgotPassword: string;
    checkResetCode: string;
    resetPassword: string;
    changePassword: string;
  };
}

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  isAuthenticated = this.tokenStore.ready.pipe(
    skipWhile((ready) => !ready),
    switchMap(() => this.tokenStore.token),
    map((token) => !!token)
  );
  user = new BehaviorSubject<User | undefined | null>(undefined);

  get currentUser() {
    return this.user.value;
  }

  constructor(
    @Inject(AUTH_CONFIG) private readonly config: AuthConfig,
    private http: HttpClient,
    private store: Store,
    private tokenStore: TokenStorageService
  ) {
    this.store
      .select(selectAuthenticatedUser)
      .pipe(skipWhile((user) => !user))
      .subscribe((user) => this.user.next(user));
  }

  login(data: Omit<LoginPayload, 'device_name'>) {
    return this.http.post<LoginResponse>(
      this.config.routes.login,
      <LoginPayload>{
        ...data,
        device_name: this.getDeviceName(),
      },
      { context: authContext('login') }
    );
  }

  me() {
    return this.http.get<MeResponse>(this.config.routes.me, { context: authContext('me') });
  }

  logout() {
    return this.http.post<LogoutResponse>(this.config.routes.logout, null, { context: authContext('logout') });
  }

  register(data: RegisterPayload) {
    return this.http.post<RegisterResponse>(this.config.routes.register, data, { context: authContext('register') });
  }

  forgotPassword(data: ForgotPasswordPayload) {
    return this.http.post<ForgotPasswordResponse>(this.config.routes.forgotPassword, data, { context: authContext('forgotPassword') });
  }

  checkResetCode(data: CheckResetCodePayload) {
    return this.http.post<CheckResetCodeResponse>(this.config.routes.checkResetCode, data, { context: authContext('checkResetCode') });
  }

  resetPassword(data: ResetPasswordPayload) {
    return this.http.post<ResetPasswordResponse>(this.config.routes.resetPassword, data, { context: authContext('resetPassword') });
  }

  changePassword(data: ChangePasswordPayload) {
    return this.http.post<ChangePasswordResponse>(this.config.routes.changePassword, data, { context: authContext('changePassword') });
  }

  protected getDeviceName() {
    return navigator.userAgent;
  }
}
