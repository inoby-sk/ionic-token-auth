import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { filter, map, retry } from 'rxjs';
import { selectAuthErrors } from '../state/auth.selectors';

@Injectable({
  providedIn: 'root',
})
export class FormErrorService {
  constructor(private store: Store) {}

  invalidClass(control: string) {
    return this.hasError(control).pipe(map((hasError) => (hasError ? 'ion-invalid' : '')));
  }

  hasError(control: string) {
    return this.store.select(selectAuthErrors).pipe(map((error) => !!(error?.errors?.[control] ?? false)));
  }

  controlError(control: string) {
    return this.store.select(selectAuthErrors).pipe(map((error) => error?.errors?.[control]?.join('') ?? undefined));
  }

  errorMessage() {
    return this.store.select(selectAuthErrors).pipe(map((e) => e?.message));
  }
}
