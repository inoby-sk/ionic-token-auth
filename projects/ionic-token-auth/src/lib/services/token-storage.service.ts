import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage-angular';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class TokenStorageService {
  private tokenKey = '__auth_token';
  private storage: Storage | null = null;

  public ready = new BehaviorSubject<boolean>(false);
  public token = new BehaviorSubject<string | undefined>(undefined);

  constructor(private _storage: Storage) {
    this.init();
  }

  async init() {
    this.storage = await this._storage.create();

    const token = await this.storage.get(this.tokenKey);

    this.token.next(token);
    this.ready.next(true);
  }

  public saveToken(token?: string) {
    this.storage?.set(this.tokenKey, token);
    this.token.next(token);
  }

  public removeToken() {
    this.storage?.remove(this.tokenKey);
    this.token.next(undefined);
  }

  public getToken() {
    return this.token.value;
  }
}
