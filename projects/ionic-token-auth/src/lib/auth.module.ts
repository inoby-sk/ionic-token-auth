import { APP_INITIALIZER, ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule, ROUTES, Routes } from '@angular/router';
import { LoginPage } from './pages/login/login.page';
import { AUTH_CONFIG_PROVIDER } from './providers';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { BaseUrlInterceptor } from './interceptors/base-url.interceptor';
import { Store, StoreModule } from '@ngrx/store';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { AUTH_STATE_NAME } from './state/auth.state';
import { authReducer } from './state/auth.reducer';
import { ReactiveFormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { AuthEffects } from './state/auth.effects';
import { RegisterPage } from './pages/register/register.page';
import { IonicModule } from '@ionic/angular';
import { AuthLayout } from './components/layout';
import { AuthCard } from './components/auth-card';
import { CommonModule } from '@angular/common';
import { ForgotPasswordPage } from './pages/forgot-password/forgot-password.page';
import { CheckResetCodePage } from './pages/check-code/check-code.page';
import { ResetPasswordPage } from './pages/reset-password/reset-password.page';
import { TokenInterceptor } from './interceptors/token.interceptor';
import { GuestGuard } from './guards/guest.guard';
import { AuthService } from './services/auth.service';
import { FormErrors } from './components/form-errors';
import { ChangePasswordPage } from './pages/change-password/change-password.page';
import { AuthGuard } from './guards/auth.guard';
import { reLogin } from './state/auth.actions';

export const defaultAuthRoutes: Routes = [
  {
    path: 'login',
    component: LoginPage,
    canActivate: [GuestGuard],
  },
  {
    path: 'register',
    component: RegisterPage,
    canActivate: [GuestGuard],
  },
  {
    path: 'forgot-password',
    component: ForgotPasswordPage,
    canActivate: [GuestGuard],
  },
  {
    path: 'check-reset-code',
    component: CheckResetCodePage,
    canActivate: [GuestGuard],
  },
  {
    path: 'reset-password',
    component: ResetPasswordPage,
    canActivate: [GuestGuard],
  },
  {
    path: 'change-password',
    component: ChangePasswordPage,
    canActivate: [AuthGuard],
  },
];

@NgModule({
  declarations: [
    LoginPage,
    RegisterPage,
    ForgotPasswordPage,
    CheckResetCodePage,
    ResetPasswordPage,
    AuthLayout,
    AuthCard,
    FormErrors,
    ChangePasswordPage,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule,
    StoreRouterConnectingModule,
    StoreModule.forFeature(AUTH_STATE_NAME, authReducer),
    EffectsModule.forFeature([AuthEffects]),
  ],
  exports: [
    CommonModule,
    LoginPage,
    RegisterPage,
    ForgotPasswordPage,
    CheckResetCodePage,
    ResetPasswordPage,
    RouterModule,
    FormErrors,
    ChangePasswordPage,
  ],
})
export class AuthModule {
  static forRoot(config?: { routes?: Routes }): ModuleWithProviders<AuthModule> {
    return {
      ngModule: AuthModule,
      providers: [
        AUTH_CONFIG_PROVIDER,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: BaseUrlInterceptor,
          multi: true,
        },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: TokenInterceptor,
          multi: true,
        },
        {
          provide: ROUTES,
          useValue: config?.routes || defaultAuthRoutes,
          multi: true,
        },
        {
          provide: APP_INITIALIZER,
          useFactory: (store: Store) => () => store.dispatch(reLogin()),
          deps: [Store],
          multi: true,
        },
      ],
    };
  }
}
