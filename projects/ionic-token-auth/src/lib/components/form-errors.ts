import { Component } from '@angular/core';
import { FormErrorService } from '../services/form-error.service';

@Component({
  selector: 'inoby-form-errors',
  template: `
    <div *ngIf="error$ | async as error" class="form-errors-container ion-padding ion-margin-bottom">
      {{ error }}
    </div>
  `,
  styles: [
    `
      .form-errors-container {
        background: rgba(255, 0, 0, 0.1);
        border-radius: 4px;
      }
    `,
  ],
})
export class FormErrors {
  error$ = this.errors.errorMessage();

  constructor(private errors: FormErrorService) {}
}
