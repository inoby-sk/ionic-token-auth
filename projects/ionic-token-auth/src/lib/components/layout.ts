import { Component, Input } from '@angular/core';

@Component({
  selector: 'inoby-auth-layout',
  template: `
    <ion-header>
      <ion-toolbar>
        <ion-buttons slot="start">
          <ion-back-button></ion-back-button>
        </ion-buttons>
        <ion-title>{{ title }}</ion-title>
      </ion-toolbar>
    </ion-header>
    <ion-content class="ion-padding" [fullscreen]="true">
      <div class="main-content">
        <ng-content></ng-content>
      </div>
    </ion-content>
  `,
  styles: [
    `
      ion-content {
        height: 100vh;
      }
      .main-content {
        height: 100%;
        display: flex;
        align-items: center;
        justify-content: center;
        transform: translateY(-30px);
      }
    `,
  ],
})
export class AuthLayout {
  @Input() title: string = '';
  constructor() {}
}
