import { Component, Input } from '@angular/core';

@Component({
  selector: 'inoby-auth-card',
  template: `
    <ion-card>
      <ion-card-header class="ion-text-center">
        <ion-card-title>{{ title }}</ion-card-title>
        <ion-card-subtitle *ngIf="subtitle">{{ subtitle }}</ion-card-subtitle>
      </ion-card-header>
      <ion-card-content>
        <ng-content></ng-content>
      </ion-card-content>
    </ion-card>
  `,
  styles: [
    `
      :host {
        width: 100%;
        max-width: 400px;
      }
    `,
  ],
})
export class AuthCard {
  @Input() title: string = '';
  @Input() subtitle!: string;

  constructor() {}
}
