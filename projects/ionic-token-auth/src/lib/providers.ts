import { HttpContext, HttpContextToken } from '@angular/common/http';
import { InjectionToken } from '@angular/core';
import { AuthConfig } from './services/auth.service';

export const AUTH_CONFIG = new InjectionToken<AuthConfig>('Config for authentication service');

export const AUTH_CONFIG_FACTORY: () => AuthConfig = () => ({
  routes: {
    login: '/login',
    me: '/me',
    logout: '/logout',
    register: '/register',
    forgotPassword: '/password/email',
    checkResetCode: '/password/check-code',
    resetPassword: '/password/reset',
    changePassword: '/password/change',
  },
  permissions: {
    admin: ['create_users', 'delete_users', 'edit_users'],
    subscriber: [],
  },
});

export const AUTH_CONFIG_PROVIDER = {
  provide: AUTH_CONFIG,
  useFactory: AUTH_CONFIG_FACTORY,
};

export const AUTH_HTTP_CONTEXT = new HttpContextToken<keyof AuthConfig['routes'] | false>(() => false);

export const authContext = (route: keyof AuthConfig['routes']) => {
  return new HttpContext().set(AUTH_HTTP_CONTEXT, route);
};
