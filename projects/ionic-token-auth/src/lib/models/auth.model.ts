import { User } from './user.model';

export interface LoginPayload {
  email: string;
  password: string;
  device_name: string;
}

export interface LoginResponse {
  token: string;
  user: User;
}

export interface LogoutResponse {
  success?: boolean;
}

export interface RegisterPayload {
  name: string;
  email: string;
  role?: string;
  password: string;
  password_confirmation: string;
}

export interface RegisterResponse {
  success?: boolean;
}

export interface ForgotPasswordPayload {
  email: string;
}

export interface ForgotPasswordResponse {
  message?: string;
}

export interface CheckResetCodePayload {
  email: string;
  code: string;
}

export interface CheckResetCodeResponse {
  message?: string;
  token?: string;
}

export interface ResetPasswordPayload {
  email: string;
  token: string;
  password: string;
  password_confirmation: string;
}

export interface ResetPasswordResponse {
  message?: string;
}

export interface MeResponse {
  user: User;
}

export interface ChangePasswordPayload {
  old_password: string;
  password: string;
  password_confirmation: string;
}

export interface ChangePasswordResponse {
  message?: string;
}
