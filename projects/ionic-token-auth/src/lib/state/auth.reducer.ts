import { Action, createReducer, on } from '@ngrx/store';
import * as authState from './auth.state';
import * as authActions from './auth.actions';
import { HttpErrorResponse } from '@angular/common/http';

const reducer = createReducer(
  authState.initialState,
  on(authActions.loginSuccess, authActions.reLoginSuccess, (state, { user }) => ({
    ...state,
    user,
  })),
  on(authActions.reLoginFailure, (state) => ({ ...state, user: null })),
  on(
    authActions.loginFailure,
    authActions.checkResetCodeFailure,
    authActions.forgotPasswordFailure,
    authActions.registerFailure,
    authActions.resetPasswordFailure,
    authActions.changePasswordFailure,
    (state, { error }) => {
      if (error instanceof HttpErrorResponse && error.error) {
        return {
          ...state,
          error: error.error,
        };
      }
      if (typeof error === 'string') {
        return {
          ...state,
          error: {
            message: error,
          },
        };
      }

      return state;
    }
  ),
  on(authActions.clearErrors, (state) => ({ ...state, error: undefined })),
  on(authActions.logoutSuccess, (state) => ({
    ...authState.initialState,
    user: null,
  })),
  on(authActions.setAuthenticatedUser, (state, { data: user }) => ({
    ...authState.initialState,
    user,
  }))
);

export function authReducer(state: authState.AuthState | undefined, action: Action) {
  return reducer(state, action);
}
