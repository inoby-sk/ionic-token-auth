import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map, exhaustMap, catchError, tap, filter, skipWhile, switchMap } from 'rxjs/operators';
import * as actions from './auth.actions';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { of, withLatestFrom } from 'rxjs';
import { TokenStorageService } from '../services/token-storage.service';
import { ROUTER_NAVIGATION } from '@ngrx/router-store';
import { Store } from '@ngrx/store';
import { ToastController } from '@ionic/angular';
import { selectAuthErrors } from './auth.selectors';

@Injectable()
export class AuthEffects {
  constructor(
    private actions$: Actions,
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    private tokenStore: TokenStorageService,
    private store: Store,
    private toastCtrl: ToastController
  ) {}

  authResult$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.login),
      exhaustMap(({ data }) =>
        this.authService.login(data).pipe(
          tap(({ token }) => this.tokenStore.saveToken(token)),
          map(({ user }) => actions.loginSuccess({ user })),

          catchError((error: any) => of(actions.loginFailure({ error })))
        )
      )
    )
  );

  authSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.loginSuccess),
        tap(() => {
          this.router.navigate([this.route.snapshot.queryParams?.['returnUrl'] || '/']);
        })
      ),
    { dispatch: false }
  );

  registerResult$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.register),
      exhaustMap(({ data }) =>
        this.authService.register(data).pipe(
          map((response) =>
            response?.success
              ? actions.registerSuccess()
              : actions.registerFailure({
                  error: 'An error occurred. Try again.',
                })
          ),
          catchError((error: any) =>
            of(
              actions.registerFailure({
                error,
              })
            )
          )
        )
      )
    )
  );

  registerSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.registerSuccess),
        tap(() => this.router.navigate(['/login']))
      ),
    { dispatch: false }
  );

  forgotPasswordResult$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.forgotPassword),
      exhaustMap(({ data }) =>
        this.authService.forgotPassword(data).pipe(
          map(() => actions.forgotPasswordSuccess({ email: data.email })),
          catchError((error: any) => of(actions.forgotPasswordFailure({ error })))
        )
      )
    )
  );

  forgotPasswordSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.forgotPasswordSuccess),
        tap(({ email }) => this.router.navigate(['/check-reset-code'], { state: { email } }))
      ),
    { dispatch: false }
  );

  checkResetCodeResult$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.checkResetCode),
      exhaustMap(({ data }) =>
        this.authService.checkResetCode(data).pipe(
          map(({ token }) =>
            token && data.email
              ? actions.checkResetCodeSuccess({ email: data.email, token })
              : actions.checkResetCodeFailure({ error: 'An error occurred. Try again.' })
          ),
          catchError((error: any) => of(actions.checkResetCodeFailure({ error })))
        )
      )
    )
  );

  checkResetCodeSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.checkResetCodeSuccess),
        tap(({ token, email }) => this.router.navigate(['/reset-password'], { state: { token, email } }))
      ),
    { dispatch: false }
  );

  resetPasswordResult$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.resetPassword),
      exhaustMap(({ data }) =>
        this.authService.resetPassword(data).pipe(
          map(() => actions.resetPasswordSuccess()),
          catchError((error: any) => of(actions.resetPasswordFailure({ error })))
        )
      )
    )
  );

  resetPasswordSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.resetPasswordSuccess),
        tap(() => {
          this.showToast('Password changed successfully');
          this.router.navigate(['/login']);
        })
      ),
    { dispatch: false }
  );

  changePasswordResult$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.changePassword),
      exhaustMap(({ data }) =>
        this.authService.changePassword(data).pipe(
          map(() => actions.changePasswordSuccess()),
          catchError((error: any) => of(actions.changePasswordFailure({ error })))
        )
      )
    )
  );

  changePasswordSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.changePasswordSuccess),
        tap(() => {
          this.showToast('Password changed successfully');
        })
      ),
    { dispatch: false }
  );

  logoutResult$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.logout),
      exhaustMap(() =>
        this.authService.logout().pipe(
          tap(() => {
            this.tokenStore.removeToken();
            this.router.navigate(['/login']);
          }),
          map(() => actions.logoutSuccess()),
          catchError(() => of(actions.logoutFailure))
        )
      )
    )
  );

  autoLogin$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.reLogin),
      switchMap(() =>
        this.tokenStore.ready.pipe(
          filter((ready) => ready),
          switchMap(() => this.tokenStore.token),
          exhaustMap((token) =>
            token
              ? this.authService.me().pipe(
                  map(({ user }) => actions.reLoginSuccess({ user })),
                  catchError(() => of(actions.reLoginFailure({ error: 'Session expired. Log in again.' })))
                )
              : of(actions.reLoginFailure({ error: null }))
          )
        )
      )
    )
  );

  autoLoginFailure$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.reLoginFailure),
        tap(({ error }) => {
          if (error) {
            this.showToast(error);
          }

          this.tokenStore.removeToken();
          this.router.navigate(['/login']);
        })
      ),
    { dispatch: false }
  );

  clearErrorsOnNavigation$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ROUTER_NAVIGATION),
      withLatestFrom(this.store.select(selectAuthErrors)),
      filter(([, errors]) => errors !== undefined),
      map(() => actions.clearErrors())
    )
  );

  private async showToast(message: string) {
    this.toastCtrl
      .create({
        message,
        duration: 4000,
      })
      .then((toast) => toast.present());
  }
}
