import { ChangePasswordPayload } from '../models/auth.model';
import { createAction, props } from '@ngrx/store';
import { LoginPayload, RegisterPayload, ForgotPasswordPayload, CheckResetCodePayload, ResetPasswordPayload } from '../models/auth.model';
import { User } from '../models/user.model';

export const login = createAction('[Auth] Login', props<{ data: Omit<LoginPayload, 'device_name'> }>());
export const loginSuccess = createAction('[Auth] Login success', props<{ user: User }>());
export const loginFailure = createAction('[Auth] Login failure', props<{ error: any }>());

export const register = createAction('[Auth] Register', props<{ data: RegisterPayload }>());
export const registerSuccess = createAction('[Auth] Register success');
export const registerFailure = createAction('[Auth] Register failure', props<{ error: any }>());

export const forgotPassword = createAction('[Auth] Forgot password', props<{ data: ForgotPasswordPayload }>());
export const forgotPasswordSuccess = createAction('[Auth] Forgot password success', props<{ email: string }>());
export const forgotPasswordFailure = createAction('[Auth] Forgot password failure', props<{ error: any }>());

export const checkResetCode = createAction('[Auth] Check reset code', props<{ data: CheckResetCodePayload }>());
export const checkResetCodeSuccess = createAction('[Auth] Check reset code success', props<{ token: string; email: string }>());
export const checkResetCodeFailure = createAction('[Auth] Check reset code failure', props<{ error: any }>());

export const resetPassword = createAction('[Auth] Reset password', props<{ data: ResetPasswordPayload }>());
export const resetPasswordSuccess = createAction('[Auth] Reset password success');
export const resetPasswordFailure = createAction('[Auth] Reset password failure', props<{ error: any }>());

export const changePassword = createAction('[Auth] Change password', props<{ data: ChangePasswordPayload }>());
export const changePasswordSuccess = createAction('[Auth] Change password success');
export const changePasswordFailure = createAction('[Auth] Change password failure', props<{ error: any }>());

export const logout = createAction('[Auth] Logout');
export const logoutSuccess = createAction('[Auth] Logout success');
export const logoutFailure = createAction('[Auth] Logout failure');

export const reLogin = createAction('[Auth] Auto re-login');
export const reLoginSuccess = createAction('[Auth] Auto re-login success', props<{ user: User }>());
export const reLoginFailure = createAction('[Auth] Auto re-login failure', props<{ error: any }>());
export const reLoginNoop = createAction('[Auth] Auto login no-op action');

export const clearErrors = createAction('[Auth] Clear errors');
export const setAuthenticatedUser = createAction('[Auth] Set authenticated user', props<{ data: User }>());
