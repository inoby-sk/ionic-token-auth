import { createFeatureSelector, createSelector, select } from '@ngrx/store';
import { AuthState, AUTH_STATE_NAME } from './auth.state';

const getAuthState = createFeatureSelector<AuthState>(AUTH_STATE_NAME);

export const selectAuth = createSelector(getAuthState, (state) => state);
export const selectAuthenticatedUser = createSelector(getAuthState, (state) => state?.user);
export const selectAuthErrors = createSelector(getAuthState, (state) => state?.error);
