import { User } from '../models/user.model';

export const AUTH_STATE_NAME = 'auth';

export interface AuthState {
  user?: User | null;
  error?: {
    message?: string;
    errors?: { [key: string]: string[] };
  };
}

export const initialState: AuthState = {};
