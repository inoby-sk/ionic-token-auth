import { Inject, Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthConfig } from '../services/auth.service';
import { AUTH_CONFIG } from '../providers';

@Injectable()
export class BaseUrlInterceptor implements HttpInterceptor {
  constructor(@Inject(AUTH_CONFIG) private readonly config: AuthConfig) {}
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.url.startsWith('/') && 'baseUrl' in this.config) {
      const apiReq = req.clone({ url: `${this.config.baseUrl}${req.url}` });
      return next.handle(apiReq);
    }
    return next.handle(req);
  }
}
