# Ionic Token Auth

## Installation

1. Add dependency

```shell
npm install @inoby-sk/ionic-token-auth
```

2. Import to project

```diff
+ import { AuthModule } from '@inoby-sk/ionic-token-auth';

  @NgModule({
    declarations: [],
    imports: [
      //
+     AuthModule.forRoot(),
    ]
  })
  ...
```
